<?php
session_start();
require_once('app/conn.php');

// some previous code...
$connection = Connection::getInstance();

$query = "SELECT * FROM usuarios WHERE correo = :mail AND clave = :password";


$email = $_POST['username'];
$password = $_POST['password'];

$statement = $connection->prepare($query);
$statement->bindValue('mail', $email, \PDO::PARAM_STR);
$statement->bindValue('password', $password, \PDO::PARAM_STR);
$statement->execute();

$result = $statement->fetch();

//var_dump(($result));
if ($result) {
    echo 'LOGIN TRUE';
    $_SESSION['logged'] = true;
    $_SESSION['user'] = $result;
}

header('location: /');
