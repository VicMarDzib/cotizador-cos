<?php
namespace App;

use App\Src\Functions;
 

require_once 'app/config.php';

// Renderizado de la vista

if ( isset($_SESSION['logged']) ) {
    Functions::get_view('index');
} else {
    Functions::get_view('login');
}