<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" /><!--  -->
  <title>Iniciar sesion cotizador</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet" />
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Merriweather&family=Poppins:wght@300&family=Roboto&display=swap" rel="stylesheet" />

  <link href="<?php echo CSS . 'styles.css' ?>" rel="stylesheet">
  </link>

</head>

<body>
  <div class="container">
    <!-- section-left-start -->

    <div class="container-left">
      <div class="wrapper-left">
        <div class="part-one">
          <h3>Bienvenido al cotizador</h3>
          <p>Ingresa tus datos</p>
        </div>
        <div class="inp">
          <form action="verificar.php" method="POST">
            <label for="" class="email">Usuario </label>
            <i class="fas fa-envelope envelope"></i>
            <input class="inp-left" name="username" type="email" placeholder="" />
            <label for="" class="pass">Contraseña </label>
            <i class="fas fa-lock pass"></i>
            <input class="inp-left" type="password" placeholder="" name="password" id="" />
            <div class="buttons">
              <button type="submit" class="btn-one">Ingresar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- section-left-end -->
  </div>
</body>

</html>