<?php

class Connection
{

    protected static $instance;

    private static $dsn = 'mysql:host=162.241.62.131;dbname=cosmycod_cotizador';

    private static $username = 'cosmycod_vic';

    private static $password = '12345';

    private function __construct()
    {
        try {
            self::$instance = new PDO(self::$dsn, self::$username, self::$password);
        } catch (PDOException $e) {
            echo "MySql Error de conexión: " . $e->getMessage();
        }
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            new Connection();
        }

        return self::$instance;
    }
}
